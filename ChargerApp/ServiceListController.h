//
//  ServiceListController.h
//  ChargerApp
//
//  Created by Pavan Jadhav on 22/03/17.
//  Copyright © 2017 Pavan Jadhav. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SCPCoreBluetoothCentralManager;
@class CBPeripheral;

@interface ServiceListController : UIViewController

@property (nonatomic, strong) SCPCoreBluetoothCentralManager *coreBluetoothManger;
@property (nonatomic, strong) CBPeripheral *connectedPeripheral;

@end
