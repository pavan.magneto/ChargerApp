//
//  CharacteristicsValueController.h
//  ChargerApp
//
//  Created by Pavan Jadhav on 22/03/17.
//  Copyright © 2017 Pavan Jadhav. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SCPCoreBluetoothCentralManager;
@class CBCharacteristic;

@interface CharacteristicsValueController : UIViewController
{
    __weak IBOutlet UILabel *hexString;
    __weak IBOutlet UILabel *asciiString;
}
@property (nonatomic, strong) SCPCoreBluetoothCentralManager *coreBluetoothManger;
@property (nonatomic, strong) CBCharacteristic *characteristic;
@end
