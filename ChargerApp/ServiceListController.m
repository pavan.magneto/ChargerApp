//
//  ServiceListController.m
//  ChargerApp
//
//  Created by Pavan Jadhav on 22/03/17.
//  Copyright © 2017 Pavan Jadhav. All rights reserved.
//

#import "ServiceListController.h"
#import "SCPCoreBluetoothCentralManager.h"
#import "CharacteristicsOfServiceController.h"

static NSString *cellIdentifier = @"cell";

@interface ServiceListController ()

@property (nonatomic, strong) CBService *selectedService;

@end

@implementation ServiceListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Service List";

    // Do any additional setup after loading the view.
}

#pragma mark - UITableviewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_connectedPeripheral services] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    CBService *service = [_connectedPeripheral services][indexPath.row];
    
    [[cell textLabel] setText:[NSString stringWithFormat:@"%@", [service UUID]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedService = [_connectedPeripheral services][indexPath.row];
    
    if(_selectedService)
    {
       // [SVProgressHUD showWithStatus:@"Searching for characteristics"];
        NSLog(@"Searching for characteristics");
        //Discover the characteristics for the selected service
        [_selectedService discoverCharacteristics:nil //If an array of CBUUIDs is given it will only look for the services with that CBUUID
                                          success:^(NSArray *discoveredCharacteristics) {
                                              NSLog(@"Characteristics found: %@", discoveredCharacteristics);
                                              
                                              if([discoveredCharacteristics count] > 0)
                                              {
                                                  dispatch_sync(dispatch_get_main_queue(), ^{
                                                   //   [SVProgressHUD showSuccessWithStatus:@"Characteristics found"];
                                                      NSLog(@"Characteristics found");
                                                      [self performSegueWithIdentifier:@"showCharacteristics" sender:self];
                                                  });
                                              }
                                              else
                                              {
                                                  dispatch_sync(dispatch_get_main_queue(), ^{
                                                 //     [SVProgressHUD showSuccessWithStatus:@"No characteristics found"];
                                                       NSLog(@"No characteristics found");
                                                  });
                                              }
                                              
                                          }
                                          failure:^(NSError *error) {
                                              NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
                                              dispatch_sync(dispatch_get_main_queue(), ^{
                                            //      [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Could not discover\n characteristics for service\n %@", [_connectedPeripheral name]]];
                                                  NSLog(@"Could not discover\n characteristics for service\n %@",[_connectedPeripheral name]);
                                              });
                                          }];
        
        //Again you can perform the same actions above without using the category methods like below
        //        [_coreBluetoothManger discoverCharacteristics:nil
        //                                           forService:_selectedService
        //                                       withPeripheral:_connectedPeripheral
        //                                              success:^(NSArray *discoveredCharacteristics) {
        //                                                  NSLog(@"Characteristics found: %@", discoveredCharacteristics);
        //
        //                                                  if([discoveredCharacteristics count] > 0)
        //                                                  {
        //                                                      dispatch_sync(dispatch_get_main_queue(), ^{
        //                                                          [SVProgressHUD showSuccessWithStatus:@"Characteristics found"];
        //                                                          [self performSegueWithIdentifier:@"showCharacteristics" sender:self];
        //                                                      });
        //                                                  }
        //                                                  else
        //                                                  {
        //                                                      dispatch_sync(dispatch_get_main_queue(), ^{
        //                                                          [SVProgressHUD showSuccessWithStatus:@"No characteristics found"];
        //                                                      });
        //                                                  }
        //
        //                                              } failure:^(NSError *error) {
        //                                                  NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        //                                                  dispatch_sync(dispatch_get_main_queue(), ^{
        //                                                      [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Could not discover\n characteristics for service\n %@", [_connectedPeripheral name]]];
        //                                                  });
        //                                              }];
    }
    else
    {
        NSLog(@"No selected service");
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *identifier = [segue identifier];
    
    if([identifier isEqualToString:@"showCharacteristics"])
    {
        CharacteristicsOfServiceController *characteristicsViewController = (CharacteristicsOfServiceController *)[segue destinationViewController];
        [characteristicsViewController setCoreBluetoothManger:_coreBluetoothManger];
        [characteristicsViewController setSelectedService:_selectedService];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





@end
