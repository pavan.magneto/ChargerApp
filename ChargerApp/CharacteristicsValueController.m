//
//  CharacteristicsValueController.m
//  ChargerApp
//
//  Created by Pavan Jadhav on 22/03/17.
//  Copyright © 2017 Pavan Jadhav. All rights reserved.
//

#import "CharacteristicsValueController.h"
#import "SCPCoreBluetoothCentralManager.h"

@interface CharacteristicsValueController ()
@property (nonatomic, strong) NSString *hexString1;
@property (nonatomic, strong) NSString *asciiString1;
@end

@implementation CharacteristicsValueController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self GetVAlues];
    self.title = @"Values of the Characteristics";

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)GetVAlues{
    if(_characteristic)
    {
    //    [SVProgressHUD showWithStatus:@"Reading characteristic value"];
        NSLog(@"Reading characteristic value");
        
        //Set the didUpdateValueBlock to get the new values as they are broadcasted
        [self.characteristic setDidUpdateValueBlock:^(NSData *updatedValue) {
            
            //As the values come bac as a data we need to convert it to a Hex string
            self.hexString1 = [updatedValue hexString];

            //Now we need to convert this Hex string into an ASCII string
            self.asciiString1 = [NSString stringFromHexString:[[self.characteristic value] hexString]];
            

            
            //Update the UI with these values
            dispatch_sync(dispatch_get_main_queue(), ^{
                hexString.text = self.hexString1;
                asciiString.text = self.asciiString1;
            //    [SVProgressHUD showSuccessWithStatus:@"Characteristic value updated"];
                NSLog(@"Characteristic value updated");
            });
        }];
        [_characteristic readValue];

}
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
