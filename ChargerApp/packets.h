/*
 * packets.h
 *
 * Created: 11/30/2016 8:26:23 AM
 *  Author: JohnF
 */ 


#ifndef PACKETS_H_
#define PACKETS_H_

// Command sent to charger
#define CMD_FIRMWARE_UPDATE		0x15
#define CMD_STATUS_GET			0x21
#define CMD_FIRMWARE_DATA_BLOCK 0x26
#define CMD_FIMRWARE_CRC		0x28
#define CMD_APP_RUN_BOOTLOADER  0x34

#pragma pack(push)
#pragma pack(1)   // All structure used in sending data over BLE must be below this line.

typedef struct
{	
	uint8_t cmd;
	uint8_t len;
}command_pkt_hdr_t;

#define APP_RUN_BL_V1 (0x424f4f54)
#define APP_RUN_BL_V2 (0x4c4f4144)
#define APP_RUN_BL_V3 (0x52554e4e)
#define APP_RUN_BL_V4 (0x41505050)
typedef struct
{
	uint32_t v1; 
	uint32_t v2; 
	uint32_t v3; 
	uint32_t v4; 
}cmd_app_run_bootloader_t;

typedef struct
{
	uint16_t new_version;
	uint16_t hw_variant;
	uint16_t size_of_version;  // in blocks
}cmd_version_get_t;

#define BYTES_PER_BLOCK (16)

#define SUB_BLOCK_NUM  (4)
#define BLOCK_SIZE (FLASH_PAGE_SIZE/SUB_BLOCK_NUM)  //Should be 16
#define FIRMWARE_CRC_MASK 0x8000
typedef struct
{
	uint16_t block_num;    
	uint8_t data[BYTES_PER_BLOCK];
}cmd_firmware_data_block_t;


typedef struct 
{
	uint16_t block_num;
	uint32_t crc;
}cmd_firmware_crc_t;

typedef struct
{
	command_pkt_hdr_t hdr;
	union
	{
		uint8_t						first_data_byte;   // used for convenience 
		cmd_version_get_t			version;
		cmd_firmware_data_block_t	fw_data;
		cmd_version_get_t			status_get;
		cmd_firmware_crc_t			firwmare_block_crc;
		cmd_app_run_bootloader_t	app_run_bl;
	};
}command_pkt_t;

// response back to mobile app.
#define RESP_FIRMWARE_DATA_BLOCK_REQ   0x92 
#define FIRMWARE_LOAD_COMPLETE_FAIL    0xFFFF
#define FIRMWARE_LOAD_COMPLETE_SUCCESS 0xFFFE  //Firmware load done and success
typedef struct 
{
	uint16_t block_num;
}resp_firmware_data_block_req_t;

#define RESP_FIRMWARE_CRC_REQ  0xB6
typedef struct  
{
	uint16_t block_num;
}resp_firmware_data_crc_req;

#define RESP_STATUS_REQ 0xA5
typedef struct 
{
	uint16_t	mode;		// 'AP' = Application 'bl' = bootloader
	uint16_t	version;
	uint16_t	hw_version;	
}resp_status_t;

typedef struct
{
	command_pkt_hdr_t hdr;
	union
	{
		resp_firmware_data_block_req_t firmware_data_block_req;	
		resp_status_t				   status_req;
		resp_firmware_data_crc_req	   firmware_crc_req;
	};
}response_pkt_t;

#pragma pack(pop) // All structure used in sending data over BLE must be above this line

bool packet_cmd_get_valid(command_pkt_t *pCmd);
uint8_t packet_cmd_get_type(command_pkt_t *pCmd);
void packet_send_firmware_data_block_req(uint16_t block_num);
void packet_send_status(uint16_t mode, uint16_t version, uint16_t hw_version);
void packet_init(void);
void packet_send_firmware_crc_req(uint16_t block_num);
bool packet_validate_bl_cmd(command_pkt_t *pCmd);
#endif /* PACKETS_H_ */
