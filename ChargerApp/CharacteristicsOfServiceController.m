//
//  CharacteristicsOfServiceController.m
//  ChargerApp
//
//  Created by Pavan Jadhav on 22/03/17.
//  Copyright © 2017 Pavan Jadhav. All rights reserved.
//

#import "CharacteristicsOfServiceController.h"
#import "SCPCoreBluetoothCentralManager.h"
#import "CharacteristicsValueController.h"

@interface CharacteristicsOfServiceController ()
@property (nonatomic, strong) CBCharacteristic *selectedCharacteristic;
@end

static NSString *cellIdentifier = @"cell";
@implementation CharacteristicsOfServiceController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Characteristics";

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_selectedService characteristics] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    CBCharacteristic *characteristic = [_selectedService characteristics][indexPath.row];
    
    [[cell textLabel] setText:[NSString stringWithFormat:@"%@", [characteristic UUID]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedCharacteristic = [_selectedService characteristics][indexPath.row];
    
    [self performSegueWithIdentifier:@"showValue" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *identifier = [segue identifier];
    
    if([identifier isEqualToString:@"showValue"])
    {
        CharacteristicsValueController *characteristicValueViewController = (CharacteristicsValueController *)[segue destinationViewController];
        [characteristicValueViewController setCoreBluetoothManger:_coreBluetoothManger];
        [characteristicValueViewController setCharacteristic:_selectedCharacteristic];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
