//
//  ViewController.m
//  ChargerApp
//
//  Created by Pavan Jadhav on 22/03/17.
//  Copyright © 2017 Pavan Jadhav. All rights reserved.
//

#import "ViewController.h"
#import "SCPCoreBluetoothCentralManager.h"
#import "ServiceListController.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    __weak IBOutlet UITableView *tblList;
    __weak IBOutlet UIActivityIndicatorView *indicator;
}

@property (nonatomic, strong) SCPCoreBluetoothCentralManager *centralManger;
@property (nonatomic, strong) NSMutableArray *discoveredPeripherals;
@property (nonatomic, strong) NSMutableArray *peripheralsRSSI;
@property (nonatomic, strong) CBPeripheral *connectedPeripheral;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Init the properties
    self.centralManger = [[SCPCoreBluetoothCentralManager alloc] init];
    self.discoveredPeripherals = [@[] mutableCopy];
    self.peripheralsRSSI = [@[] mutableCopy];
    [self startUpCentralManager];
    NSLog(@"pavan...");
    self.title = @"Peripherals";
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Clean up ensures that we are disconnected and unsubsribed
    [_centralManger cleanup];
}

-(void)startAnimating{
    [indicator startAnimating];
}
-(void)stopAnimating{
    [indicator stopAnimating];
}

-(void)startUpCentralManager{
    //Start up the central manager
    [_centralManger startUpSuccess:^{
        NSLog(@"Core bluetooth manager successfully started.");
        //Once the central manager is successfully started, start scanning for peripherals
        [self scanForPeripherals];
    } failure:^(CBCentralManagerState CBCentralManagerState) {
        //Handel the error.
        NSString *message;
        switch (CBCentralManagerState) {
            case CBManagerStateUnknown:
            {
                message = @"Unknown state";
                break;
            }
            case CBManagerStateResetting:
            {
                message = @"Central manager is resetting";
                break;
            }
            case CBManagerStateUnsupported:
            {
                message = @"Your device is not supported";
                NSLog(@"Please note it will not work on a simulator");
                break;
            }
            case CBManagerStateUnauthorized:
            {
                message = @"Unauthorized";
                break;
            }
            case CBManagerStatePoweredOff:
            {
                message = @"Bluetooth is switched off";
                break;
            }
            default:
            {
                //Empty default to remove switch warning
                break;
            }
        }
        //Remove any previously found peripheral
        [self.discoveredPeripherals removeAllObjects];
        [self.peripheralsRSSI removeAllObjects];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            //       [SVProgressHUD showErrorWithStatus:message];
            NSLog(@"%@",message);
            [tblList reloadData];
        });
        NSLog(@"Error %ld", (long)CBCentralManagerState);
    }];
    
    //Set the did disconnect block to handel if the peripheral disconnects anytime during the app
    [_centralManger setDidDisconnectFromPeripheralBlock:^(CBPeripheral *peripheral) {
        NSLog(@"Did disconnect");
        //Remove any previously found peripheral
        [self.discoveredPeripherals removeAllObjects];
        [self.peripheralsRSSI removeAllObjects];
        //Call it on the main thread to pop to root view
        dispatch_sync(dispatch_get_main_queue(), ^{
            //[SVProgressHUD showErrorWithStatus:@"Disconnected from\nperipheral"];
            [[self navigationController] popToRootViewControllerAnimated:YES];
            [tblList reloadData];
            [self performSelector:@selector(scanForPeripherals) withObject:nil afterDelay:1.0];
        });
    }];
}

- (void)scanForPeripherals
{
    //  [SVProgressHUD showWithStatus:@"Searching for peripherals"];
    //   __weak SCPPeripheralsTableViewController *weakSelf = self;
    [self startAnimating];
    //Remove any previously found peripheral
    [_discoveredPeripherals removeAllObjects];
    [_peripheralsRSSI removeAllObjects];
    [tblList reloadData];
    
    //Check that the central manager is ready to scan
    if([_centralManger isReady])
    {
        //Tell the central manager to start scanning
        [_centralManger scanForPeripheralsWithServices:nil //If an array of CBUUIDs is given it will only look for the peripherals with that CBUUID
                                       allowDuplicates:NO
                                 didDiscoverPeripheral:^(CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI) {
                                     //A peripheral has been found
                                     NSLog(@"Discovered Peripheral '%@' with RSSI of %@", [peripheral name], RSSI);
                                     
                                     //To ensure we don't have duplicates
                                     if(![self.discoveredPeripherals containsObject:peripheral])
                                     {
                                         //Add it to the discoveredPeripherals array and update the UI on the main thread
                                         [self.discoveredPeripherals addObject:peripheral];
                                         [self.peripheralsRSSI addObject:RSSI];
                                         dispatch_sync(dispatch_get_main_queue(), ^{
                                             [tblList reloadData];
                                             // [SVProgressHUD dismiss];
                                             [self stopAnimating];
                                         });
                                     }
                                 }];
        NSLog(@"Scanning started");
    }
    else
    {
        NSLog(@"Central manager not ready to scan");
    }
}

#pragma mark - UITableviewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_discoveredPeripherals count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    CBPeripheral *peripheral = _discoveredPeripherals[indexPath.row];
    
    NSString *peripheralName = [peripheral name] ? [peripheral name] : @"Unidentified";
    NSNumber *peripheralRSSI = _peripheralsRSSI[indexPath.row];
    [[cell textLabel] setText:peripheralName];
    [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%@", peripheralRSSI]];
    return cell;
}

#pragma mark - UITableviewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([_centralManger isReady])
    {
        CBPeripheral *selectedPeripheral = _discoveredPeripherals[indexPath.row];
        [self startAnimating];
       // [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Connecting to\n%@", [selectedPeripheral name]]];
        NSLog(@"Connecting to\n%@", [selectedPeripheral name]);
        //Check we have a peripheral selected
        if(selectedPeripheral)
        {
            //Attempt to connect to the peripheral
            [selectedPeripheral connectSuccess:^(CBPeripheral *peripheral) {
                //Successfuly connected
                NSLog(@"Connected to peripheral '%@'", [peripheral name]);
                self.connectedPeripheral = peripheral;
                
                //Stop the scanning as we don't need to look for anymore
                [_centralManger stopScanning];
                NSLog(@"Scanning stopped");
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                  //  [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Searching for\nservices for\n%@", [peripheral name]]];
                     NSLog(@"Searching for\nservices for\n%@", [peripheral name]);
                });
                
                //Discover the services for the newly connected peripheral
            
                [peripheral setDidWriteValueForCharacteristicBlock:^(NSData *returnedValue) {
                   
                }];
                
                [peripheral discoverServices:nil
                                     success:^(NSArray *discoveredServices) {
                                         NSLog(@"Services found %@", discoveredServices);
                                         [self stopAnimating];
                                         if([discoveredServices count] > 0)
                                         {
                                             //Move to show the services
                                             dispatch_sync(dispatch_get_main_queue(), ^{
                                             //    [SVProgressHUD showSuccessWithStatus:@"Services found"];
                                                 [self stopAnimating];
                                                 [self performSegueWithIdentifier:@"showServices" sender:self];
                                                  NSLog(@"Services found");
                                             });
                                         }
                                         else
                                         {
                                             dispatch_sync(dispatch_get_main_queue(), ^{
                                               //  [SVProgressHUD showErrorWithStatus:@"No services found"];
                                                 [peripheral performSelector:@selector(disconnect) withObject:nil afterDelay:1.0];
                                                 NSLog(@"No Services found");
                                             });
                                         }
                                     }
                                     failure:^(NSError *error) {
                                         NSLog(@"Error discovering services for peripheral '%@'", [peripheral name]);
                                     }];
                
            } failure:^(CBPeripheral *peripheral, NSError *error) {
                NSLog(@"Failed connecting to Peripheral '%@'. Error : %@", [peripheral name], [error localizedDescription]);
                dispatch_sync(dispatch_get_main_queue(), ^{
                 //   [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Failed to connect to\n%@", [peripheral name]]];
                    NSLog(@"Failed to connect to\n%@", [peripheral name]);
                    [self stopAnimating];
                });
            }];
            
            //Below is a second way of perfoming the same taks above but using the central manager rather than the cateogry methods
            //            [_centralManger connectToPeripheral:selectedPeripheral
            //                                        success:^(CBPeripheral *peripheral) {
            //                                            NSLog(@"Connected to peripheral '%@'", [peripheral name]);
            //                                            weakSelf.connectedPeripheral = peripheral;
            //
            //                                            [_centralManger stopScanning];
            //                                            NSLog(@"Scanning stopped");
            //
            //                                            dispatch_sync(dispatch_get_main_queue(), ^{
            //                                                [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"Searching for services for\n%@", [peripheral name]]];
            //                                            });
            //
            //                                            [weakSelf.centralManger discoverServices:nil
            //                                                                       ForPeripheral:peripheral
            //                                                                             success:^(NSArray *discoveredServices) {
            //                                                                                 NSLog(@"Services found %@", discoveredServices);
            //
            //                                                                                 if([discoveredServices count] > 0)
            //                                                                                 {
            //                                                                                     //Move to show the services
            //                                                                                     dispatch_sync(dispatch_get_main_queue(), ^{
            //                                                                                         [SVProgressHUD showSuccessWithStatus:@"Services found"];
            //                                                                                         [weakSelf performSegueWithIdentifier:@"showServices" sender:weakSelf];
            //                                                                                     });
            //                                                                                 }
            //                                                                                 else
            //                                                                                 {
            //                                                                                     dispatch_sync(dispatch_get_main_queue(), ^{
            //                                                                                         [SVProgressHUD showErrorWithStatus:@"No services found"];
            //                                                                                         [peripheral performSelector:@selector(disconnect) withObject:nil afterDelay:1.0];
            //                                                                                     });
            //                                                                                 }
            //                                                                             }
            //                                                                             failure:^(NSError *error) {
            //                                                                                 NSLog(@"Error discovering services for peripheral '%@'", [peripheral name]);
            //                                                                             }];
            //                                        }
            //                                        failure:^(CBPeripheral *peripheral, NSError *error) {
            //                                            NSLog(@"Failed connecting to Peripheral '%@'. Error : %@", [peripheral name], [error localizedDescription]);
            //                                            dispatch_sync(dispatch_get_main_queue(), ^{
            //                                                [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Failed to connect to\n%@", [peripheral name]]];
            //                                            });
            //                                        }];
        }
        else
        {
            NSLog(@"No selected peripheral");
      //      [SVProgressHUD showErrorWithStatus:@"No selected peripheral"];
        }
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString *identifier = [segue identifier];
    
    if([identifier isEqualToString:@"showServices"])
    {
        ServiceListController *servicesViewController = (ServiceListController *)[segue destinationViewController];
        [servicesViewController setCoreBluetoothManger:_centralManger];
        [servicesViewController setConnectedPeripheral:_connectedPeripheral];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
