//
//  main.m
//  ChargerApp
//
//  Created by Pavan Jadhav on 22/03/17.
//  Copyright © 2017 Pavan Jadhav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
